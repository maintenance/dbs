#!/bin/bash


# Дергаем список партиций
parts=`/usr/bin/clickhouse-client -d gaia -q "SELECT partition, table, database FROM system.parts WHERE active AND database = 'gaia' FORMAT TabSeparatedRaw;" | awk '{print $1}'`

# Пишем список партиций в файл
echo $parts | sed "s/ /\\n/g" > /opt/sas/backup_database/parts

# Подгружаем список партиций из файла
all_parts=`cat /opt/sas/backup_database/parts`

# Циклом подставляем партиции
array1=(`echo "$all_parts"`)
FIRST=1
i=0
for VER in ${array1[@]}
do
  if [ ${FIRST} -eq 1 ] ; then
     FIRST=0
  fi

# Делаем бэкапы в /opt/........./shadow/
  /usr/bin/clickhouse-client -d gaia -q "ALTER TABLE gaia.event_search_local FREEZE PARTITION ${array1[i]};"

  i=${i}+1

done

# Жмем бэкап
function compress {
cd /opt/sas/backup_4transfer
/usr/bin/nice -n 19 /usr/bin/tar -cf clickhouse_shard-1_ch-dwh-l1_$(date +%F_%H-%M-%S).tar.bz2 --use-compress-prog=pbzip2 /opt/sas/data/shadow/* /opt/ssd/data/shadow/* /opt/sas/clickhouse_directory/*
}

# Чистим shadow
function clear_shadow {
/usr/bin/rm -rf /opt/sas/data/shadow/*
/usr/bin/rm -rf /opt/ssd/data/shadow/*
}

compress
clear_shadow
