#!/bin/bash

function clean_files {
/usr/bin/rm -rf /opt/sas/backup_4transfer/clickhouse_*
}

function cleaner_exit {
exit 0
}

search_file=`ls -la /opt/sas/backup_4transfer/ | grep "clickhouse_" | wc -l`
if [ $search_file -lt 1 ]; then
  cleaner_exit
else
  clean_files
fi
